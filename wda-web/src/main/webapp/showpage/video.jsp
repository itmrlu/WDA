<%@page import="com.farm.wda.inter.WdaAppInter"%>
<%@page import="com.farm.wda.util.AppConfig"%>
<%@page import="java.io.File"%>
<%@page import="com.farm.wda.Beanfactory"%>
<%@ page language="java" pageEncoding="utf-8"%>
<html lang="zh-CN">

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<head>
<base href="<%=basePath%>">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><%=AppConfig.getString("config.web.title")%></title>
<script src="js/jquery11.3.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet">
<script src="lib/dplayer/DPlayer.min.js"></script>
</head>
<%
	WdaAppInter wad = Beanfactory.getWdaAppImpl();
%>
<body style="background-color: #8a8a8a;">
	<jsp:include page="/commons/head.jsp"></jsp:include>
	<div class="container">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<div class="panel panel-default">
					<div class="panel-heading">视频预览</div>
					<div class="panel-body" style="text-align: center;">
						<div id="player1" class="dplayer" style="height: 400px;"></div>
					</div>
				</div>
			</div>
			<div class="col-md-1"></div>
		</div>
	</div>
</body>
<script type="text/javascript">
	$(function() {
		var dp = new DPlayer({
			element : document.getElementById('player1'),
			autoplay : false,
			theme : '#FADFA3',
			loop : false,
			lang : 'zh-cn',
			screenshot : false,
			hotkey : true,
			preload : 'auto',
			video : {
				url : '<%=wad.getUrl(request.getParameter("key"), "OVIDEO")%>'
			},
			contextmenu : []
		});
	});
</script>
</html>